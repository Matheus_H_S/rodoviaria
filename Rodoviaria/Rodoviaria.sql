create database rodoviaria;
use rodoviaria;

CREATE TABLE passageiro(
idpassageiro int NOT null auto_increment,
rg varchar(14) NOT null,
cpf varchar(10) NOT null,
nome varchar(254) NOT null,
senha varchar(8) NOT null,
login varchar(254) NOT null,
genero varchar(254) NOT null,
endereço varchar(254) NOT null,
email varchar(100) NOT null,
telefone varchar(17) NOT null,
PRIMARY KEY(idpassageiro)
);
package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFLogin extends JFrame {

	private JPanel contentPane;
	private JTextField textUsuario;
	private JPasswordField passwordSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFLogin frame = new JFLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFLogin() {
		setTitle("SisRodovi\u00E1ria - Tela de Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBemvindo = new JLabel("SIS RODOVI\u00C1RIA - BEM VINDO!");
		lblBemvindo.setFont(new Font("Poor Richard", Font.PLAIN, 15));
		lblBemvindo.setBounds(10, 11, 216, 31);
		contentPane.add(lblBemvindo);
		
		JLabel lblInformeseusdados = new JLabel("Informe suas credenciais de acesso");
		lblInformeseusdados.setBounds(10, 53, 196, 14);
		contentPane.add(lblInformeseusdados);
		
		JLabel lblUsuario = new JLabel("Usu\u00E1rio:");
		lblUsuario.setBounds(10, 78, 55, 14);
		contentPane.add(lblUsuario);
		
		textUsuario = new JTextField();
		textUsuario.setBounds(59, 75, 365, 20);
		contentPane.add(textUsuario);
		textUsuario.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setBounds(10, 103, 46, 14);
		contentPane.add(lblSenha);
		
		passwordSenha = new JPasswordField();
		passwordSenha.setBounds(59, 100, 365, 20);
		contentPane.add(passwordSenha);
		
		JButton btnAcessar = new JButton("Acessar");
		btnAcessar.setBounds(29, 160, 89, 23);
		contentPane.add(btnAcessar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(320, 160, 89, 23);
		contentPane.add(btnCancelar);
		
		JButton btnCadastrese = new JButton("Cadastre-se");
		btnCadastrese.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnCadastrese.setBounds(29, 205, 108, 23);
		contentPane.add(btnCadastrese);
		
		JButton btnRecuperarSenha = new JButton("Recuperar senha");
		btnRecuperarSenha.setBounds(294, 194, 115, 23);
		contentPane.add(btnRecuperarSenha);
	}
}

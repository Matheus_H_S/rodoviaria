package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.ScrollPaneConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class JFListarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JTPassageiro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarPassageiros frame = new JFListarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarPassageiros() {
		
		JLabel lblListarFilmes = new JLabel("Listar filmes");
		getContentPane().add(lblListarFilmes, BorderLayout.NORTH);
		
		JTextArea textArea = new JTextArea();
		getContentPane().add(textArea, BorderLayout.CENTER);
		
		JButton btnNewButton = new JButton("New button");
		getContentPane().add(btnNewButton, BorderLayout.SOUTH);
		setTitle("Listar Passageiros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 651, 509);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblListarPassageiros = new JLabel("Listar Passageiros");
		lblListarPassageiros.setFont(new Font("Dialog", Font.BOLD, 16));
		lblListarPassageiros.setBounds(10, 11, 205, 21);
		contentPane.add(lblListarPassageiros);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 44, 613, 276);
		contentPane.add(scrollPane);
		
		JTPassageiro = new JTable();
		JTPassageiro.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"ID", "Nome", "Telefone", "E-mail", "Endere\u00E7o"
			}
		));
		scrollPane.setViewportView(JTPassageiro);
		
		JButton btnCadastarPassageiro = new JButton("Cadastrar Passageiro");
		btnCadastarPassageiro.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				JFCadastroPassageiro cf = new JFCadastroPassageiro();
				cf.setVisible(true);
			}
		});
		btnCadastarPassageiro.setBounds(10, 354, 159, 26);
		contentPane.add(btnCadastarPassageiro);
		
		JButton btnAlterarPassageiro = new JButton("Alterar Passageiro");
		btnAlterarPassageiro.setBounds(181, 354, 150, 26);
		contentPane.add(btnAlterarPassageiro);
		public void actionPerformed(ActionEvent arg0) {
			//verificar a linha selecionada//
			if(JTPassageiros.getSelectedRow()!= -1) {
				JFatualizarPassageiro af = new JFAtualizarPassageiro((int)JTPassageiros.getValueAT(JFpassageiro.getSelectedRow(),0));
				af.setVisible(true);
				
			}else {
				JOptionPane.showMessageDialog(null, "Selecione um passageiro");
			}
		
			readJTable();
		});
		
		JButton btnExcluirPassageiro = new JButton("Excluir Passageiro");
		btnExcluirPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(JTPassageiro.getSelectedRow() != -1) {
				int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o filme selecionado?", "Exclus�o", JOptionPane.YES_NO_OPTION);
				if(opcao == 0) {
					PassageiroDAO dao = new PassageiroDAO();
					Passageiro p = new Passageiro();
					p.setIdpassageiro((int)JTPassageiro.getValueAt(JTPassageiro.getSelectedRow(), 0));
					dao.delete(p);
				}
				}else {
					JOptionPane.showMessageDialog(null, "Selecione um filme!");
				}
				readJTable();
			}
		});
		btnExcluirPassageiro.setBounds(349, 354, 150, 26);
		contentPane.add(btnExcluirPassageiro);
		
		readJTable();
	}
	
	public void readJTable(){
		DefaultTableModel modelo = (DefaultTableModel) JTPassageiro.getModel();
		modelo.setNumRows(0);
		PassageiroDAO fdao = new PassageiroDAO();
		for(Passageiro f : fdao.read()) {
			modelo.addRow(new Object[] {
					f.getIdpassageiro(),
					f.getNome(),
					f.getTelefone(),
					f.getEmail(),
					f.getEndere�o()
					
			});
		}
	}
	
}

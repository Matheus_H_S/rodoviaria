package view;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFCadastroPassageiro extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtRG;
	private JTextField txtCPF;
	private JTextField txtEmail;
	private JTextField textField;
	private JTextField txtGenero;
	private JTextField txtTelefone;
	private JTextField txtLogin;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadastroPassageiro frame = new JFCadastroPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastroPassageiro() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 565, 512);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cadastar Passageiro");
		lblNewLabel.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblNewLabel.setBounds(10, 11, 163, 19);
		contentPane.add(lblNewLabel);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblNome.setBounds(10, 41, 57, 19);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 71, 525, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblRG = new JLabel("RG");
		lblRG.setBounds(10, 102, 46, 14);
		contentPane.add(lblRG);
		
		txtRG = new JTextField();
		txtRG.setBounds(10, 127, 163, 20);
		contentPane.add(txtRG);
		txtRG.setColumns(10);
		
		JLabel lblCPF = new JLabel("CPF");
		lblCPF.setBounds(205, 102, 46, 14);
		contentPane.add(lblCPF);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(205, 127, 163, 20);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setBounds(10, 158, 57, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(10, 183, 525, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblEnderešo = new JLabel("Endere\u00E7o");
		lblEnderešo.setBounds(10, 214, 71, 14);
		contentPane.add(lblEnderešo);
		
		textField = new JTextField();
		textField.setBounds(10, 239, 525, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblGenero = new JLabel("Genero");
		lblGenero.setBounds(402, 102, 46, 14);
		contentPane.add(lblGenero);
		
		txtGenero = new JTextField();
		txtGenero.setBounds(402, 127, 86, 20);
		contentPane.add(txtGenero);
		txtGenero.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(10, 270, 71, 14);
		contentPane.add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(10, 295, 163, 20);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setBounds(205, 270, 46, 14);
		contentPane.add(lblLogin);
		
		txtLogin = new JTextField();
		txtLogin.setBounds(205, 295, 147, 20);
		contentPane.add(txtLogin);
		txtLogin.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setBounds(373, 270, 46, 14);
		contentPane.add(lblSenha);
		
		txtSenha = new JPasswordField();
		txtSenha.setBounds(373, 295, 115, 20);
		contentPane.add(txtSenha);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				Passageiro p = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				
				p.setNome(txtNome.getName());
				p.setCpf(txtCPF.getName());
				p.setEmail(txtEmail.getName());
				p.setEnderešo(textField.getName());
				p.setGenero(txtGenero.getName());
				p.setLogin(txtLogin.getName());
				p.setNome(txtNome.getName());
				p.setRg(txtRG.getName());
				p.setSenha(txtSenha.getName());
				p.setTelefone(txtTelefone.getName());
			
				dao.create(p);
				dispose();
			}
		});
		btnCadastrar.setBounds(10, 338, 89, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				txtNome.setName(null);
				txtCPF.setName(null);
				txtEmail.setName(null);
				textField.setName(null);
				txtGenero.setName(null);
				txtLogin.setName(null);
				txtNome.setName(null);
				txtRG.setName(null);
				txtSenha.setName(null);
				txtTelefone.setName(null);
				
			}
		});
		btnLimpar.setBounds(216, 338, 89, 23);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(416, 338, 89, 23);
		contentPane.add(btnCancelar);
	}
}

package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.Passageiro;

public class PassageiroDAO {

	public void create(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt= con.prepareStatement("INSERT INTO passageiro (rg, cpf, nome, senha, login, genero, endereço, email, telefone) Values (?,?,?,?,?,?,?,?,?");
			stmt.setString(1, p.getRg());
			stmt.setString(2, p.getCpf());
			stmt.setString(3, p.getNome());
			stmt.setString(4, p.getSenha());
			stmt.setString(5, p.getLogin());
			stmt.setString(6, p.getGenero());
			stmt.setString(7, p.getEndereço());
			stmt.setString(8, p.getEmail());
			stmt.setString(9, p.getTelefone());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");			
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);	
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public List<Passageiro> read(){
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt =null;
		ResultSet rs = null;
		List<Passageiro> passageiro = new ArrayList<>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Passageiro f = new Passageiro();
				f.setIdpassageiro(rs.getInt("idpassageiro"));
				f.setRg(rs.getString("rg"));
				f.setCpf(rs.getString("cpf"));
				f.setNome(rs.getString("nome"));
				f.setSenha(rs.getString("senha"));
				f.setLogin(rs.getString("login"));
				f.setGenero(rs.getString("genero"));
				f.setEndereço(rs.getString("endereço"));
				f.setEmail(rs.getString("email"));
				f.setTelefone(rs.getString("telefone"));
				passageiro.add(f);
				
				
				
			}
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informaçoes do BD" + e);
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return passageiro;
	}
	
	
	public void delete(Passageiro p) {
		
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			
			stmt = con.prepareStatement("DELETE FROM passageiro WHERE idpassageiro=?");
			stmt.setInt(1, p.getIdpassageiro());
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Passagero excluido com sucesso!!");			
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir: " + e);
		}finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
		
		
	}
	
	public Passageiro read(int id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Passageiro p = new Passageiro();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro WHERE idPassageiro=? LIMIT 1;");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if(rs != null && rs.next()) {
				p.setIdpassageiro(rs.getInt("idpassageiro"));
				p.setRg(rs.getString("rg"));
				p.setCpf(rs.getString("cpf"));
				p.setNome(rs.getString("nome"));
				p.setSenha(rs.getString("senha"));
				p.setLogin(rs.getString("login"));
				p.setGenero(rs.getString("genero"));
				p.setEndereço(rs.getString("endereço"));
				p.setEmail(rs.getString("email"));
				p.setTelefone(rs.getString("telefone"));							
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return p;
	}
	
	public void update(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt= con.prepareStatement("UPDATE passageiro SET rg=?, cpf=?, nome=?, senha=?, login=?, genero=?, endereço=?, email=?, telefone=? WHERE idpassageiro=?;");
			stmt.setString(1, p.getRg());
			stmt.setString(2, p.getCpf());
			stmt.setString(3, p.getNome());
			stmt.setString(4, p.getSenha());
			stmt.setString(5, p.getLogin());
			stmt.setString(6, p.getGenero());
			stmt.setString(7, p.getEndereço());
			stmt.setString(8, p.getEmail());
			stmt.setString(9, p.getTelefone());
			stmt.setInt(10, p.getIdpassageiro());
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Alterado com sucesso!");			
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);	
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
}
